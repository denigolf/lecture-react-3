const userRoutes = require('./userRoutes');
const messageRoutes = require('./messageRoutes');
// const authRoutes = require('./authRoutes');

module.exports = (app) => {
  app.use('/api/users', userRoutes);
  app.use('/api/messages', messageRoutes);
  // app.use('/api/auth', authRoutes);
};
