const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
  getAll() {
    return MessageRepository.getAll();
  }

  getOne(id) {
    return MessageRepository.getOne(id);
  }

  create(message) {
    MessageRepository.create(message);
  }

  update(id, dataToUpdate) {
    return MessageRepository.update(id, dataToUpdate);
  }

  delete(id) {
    return MessageRepository.delete(id);
  }
}

module.exports = new MessageService();
