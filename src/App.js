import './App.css';
import Chat from './containers/Chat/';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import LoginPage from './containers/LoginPage/';
import MessageEditor from './containers/MessageEditor/';
import Navigation from './containers/Navigation';
import UserList from './containers/users';
import UserPage from './containers/userPage';
import { useSelector } from 'react-redux';

function App() {
  const chatData = 'http://localhost:3050/api/messages';
  const isShown = useSelector((state) => state.userPage.isShown);
  const isAuthed = useSelector((state) => state.login.isAuthed);
  const isAdmin = useSelector((state) => state.login.isAdmin);

  return (
    <BrowserRouter>
      <div className="App">
        {isAuthed ? <Navigation /> : null}
        <Switch>
          <Route path="/login" component={LoginPage} />
          {isAuthed ? (
            <Route
              path="/chat"
              render={(props) => <Chat url={chatData} {...props} />}
            />
          ) : (
            <Redirect to="/login" />
          )}

          <Route path="/edit" component={MessageEditor} />
          <Route
            path="/user-edit"
            render={(props) => (
              <>
                <UserPage {...props} />
                {!isShown ? (
                  <Redirect to="/user" />
                ) : (
                  <Redirect to="/user-edit" />
                )}
              </>
            )}
          />
          {isAdmin ? (
            <Route
              path="/user"
              render={(props) => (
                <>
                  <UserList {...props} />
                  <UserPage {...props} />
                </>
              )}
            />
          ) : null}
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
