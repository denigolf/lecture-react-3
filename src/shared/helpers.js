export const formatDate = (date, language) => {
  const timeOptions = {
    hour: '2-digit',
    minute: '2-digit',
  };

  const dateOptions = {
    month: 'long',
    year: 'numeric',
    day: '2-digit',
  };

  const _time = new Date(date).toLocaleString(language, timeOptions);
  const _date = new Date(date).toLocaleString(language, dateOptions);

  return `${_time}, ${_date}`;
};

export const formatDateHeader = (date) => {
  const newDate = new Date(date);
  const dateFormated = `${newDate.getDate().toString().padStart(2, '0')}.${(
    newDate.getMonth() + 1
  )
    .toString()
    .padStart(2, '0')}.${newDate
    .getFullYear()
    .toString()
    .padStart(4, '0')} ${newDate
    .getHours()
    .toString()
    .padStart(2, '0')}:${newDate.getMinutes().toString().padStart(2, '0')}`;

  return dateFormated;
};

export const formatDateMessage = (date) => {
  const newDate = new Date(date);
  const dateFormated = `${newDate
    .getHours()
    .toString()
    .padStart(2, '0')}:${newDate.getMinutes().toString().padStart(2, '0')}`;

  return dateFormated;
};

export const formatDateDivider = (date) => {
  const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];

  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  const newDate = new Date(date);

  const day = days[newDate.getDay()];
  const month = months[newDate.getMonth()];

  const dateFormated = `${day}, ${newDate
    .getDate()
    .toString()
    .padStart(2, '0')} ${month}`;
  return dateFormated;
};
