import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Preloader from '../../shared/components/Preloader/Preloader';
import Header from './components/Header/Header';
import MessageList from './components/MessageList/MessageList';
import MessageInput from './components/MessageInput/MessageInput';
import { getMessages, openModal } from './actions';
import { Redirect } from 'react-router-dom';

const Chat = ({ url }) => {
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.chat.messages);
  const isLoading = useSelector((state) => state.chat.preloader);
  const editModal = useSelector((state) => state.chat.editModal);

  const openModalHandler = (e) => {
    dispatch(openModal(e));
  };

  useEffect(() => {
    dispatch(getMessages(url));
    window.addEventListener('keydown', openModalHandler);

    return () => window.removeEventListener('keydown', openModalHandler);
  }, [dispatch]);

  return isLoading ? (
    <Preloader />
  ) : (
    <div className="chat">
      <Header messages={messages} />
      <MessageList messages={messages} />
      <MessageInput data={messages} />
      {editModal ? <Redirect to="/edit" /> : <Redirect to="/chat" />}
    </div>
  );
};

export default Chat;
