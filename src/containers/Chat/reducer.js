import {
  LOAD_MESSAGES,
  TOGGLE_PRELOADER,
  TOGGLE_EDIT_MODAL,
  DELETE_MESSAGE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  SET_CURRENT_MESSAGE_ID,
  DROP_CURRENT_MESSAGE_ID,
} from './actionTypes';

import { createReducer } from '@reduxjs/toolkit';

const initialState = {
  messages: [],
  editModal: false,
  preloader: true,
  currentMessageId: '',
};

const chat = createReducer(initialState, (builder) => {
  builder.addCase(LOAD_MESSAGES, (state, action) => {
    const { messages } = action.payload;
    state.messages = messages;
  });
  builder.addCase(TOGGLE_PRELOADER, (state) => {
    state.preloader = !state.preloader;
  });
  builder.addCase(TOGGLE_EDIT_MODAL, (state) => {
    state.editModal = !state.editModal;
  });
  builder.addCase(SET_CURRENT_MESSAGE_ID, (state, action) => {
    const { messageId } = action.payload;
    state.currentMessageId = messageId;
  });
  builder.addCase(DROP_CURRENT_MESSAGE_ID, (state) => {
    state.currentMessageId = '';
  });
  builder.addCase(SEND_MESSAGE, (state, action) => {
    const { message } = action.payload;
    if (!message.text.trim()) {
      return;
    }
    state.messages.push(message);
  });
  builder.addCase(EDIT_MESSAGE, (state, action) => {
    const { messageId, text } = action.payload;
    if (text.trim() === '') {
      return;
    }
    const messages = [...state.messages];
    const index = messages.findIndex((message) => {
      return message.id === messageId;
    });
    state.messages[index] = { ...state.messages[index], text };
    state.editModal = true;
  });
  builder.addCase(DELETE_MESSAGE, (state, action) => {
    const { id } = action.payload;
    const index = state.messages.findIndex((message) => {
      return message.id === id;
    });
    state.messages.splice(index, 1);
  });
});

export default chat;
