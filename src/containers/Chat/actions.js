import {
  LOAD_MESSAGES,
  TOGGLE_PRELOADER,
  TOGGLE_EDIT_MODAL,
  DELETE_MESSAGE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  SET_CURRENT_MESSAGE_ID,
  DROP_CURRENT_MESSAGE_ID,
} from './actionTypes';

import { v4 as uuidv4 } from 'uuid';

export const loadMessages = (messages) => {
  return {
    type: LOAD_MESSAGES,
    payload: { messages },
  };
};

export const togglePreloader = () => {
  return {
    type: TOGGLE_PRELOADER,
  };
};

export const toggleEditModal = () => {
  return {
    type: TOGGLE_EDIT_MODAL,
  };
};

export const sendMessage = (message) => {
  return {
    type: SEND_MESSAGE,
    payload: {
      message,
    },
  };
};

export const editMessage = (messageId, text) => {
  return {
    type: EDIT_MESSAGE,
    payload: {
      messageId,
      text,
    },
  };
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: { id },
  };
};

export const setCurrentMessageId = (messageId) => {
  return {
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
      messageId,
    },
  };
};

export const dropCurrentMessageId = () => {
  return {
    type: DROP_CURRENT_MESSAGE_ID,
  };
};

export const getMessages = (url) => {
  return async (dispatch, store) => {
    try {
      const { preloader } = store().chat;
      if (!preloader) {
        dispatch(togglePreloader());
      }

      const response = await fetch(url);
      const data = await response.json();

      if (data.length === 0) {
        return;
      }

      dispatch(loadMessages(data));

      dispatch(togglePreloader());
    } catch (err) {
      console.log(err);
    }
  };
};

export const createMessageMiddleware = (text) => {
  return async (dispatch) => {
    let currentTime = new Date();
    currentTime = currentTime.toJSON();
    const userId = 'my-id';
    const messageId = uuidv4();

    const message = {
      id: messageId,
      userId: userId,
      text: text,
      own: true,
      createdAt: currentTime,
    };
    try {
      if (message.text.trim()) {
        fetch('http://localhost:3050/api/messages', {
          method: 'POST',
          body: JSON.stringify(message),
          headers: { 'Content-type': 'application/json; charset=UTF-8' },
        }).then((response) => response.json());
        dispatch(sendMessage(message));
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const deleteMessageMiddleware = (messageId) => {
  return async (dispatch, store) => {
    try {
      dispatch(togglePreloader());

      fetch(`http://localhost:3050/api/messages/${messageId}`, {
        method: 'DELETE',
      }).then((response) => response.json());

      dispatch(deleteMessage(messageId));
      dispatch(togglePreloader());
    } catch (err) {
      console.log(err);
    }
  };
};

export const editMessageMiddleware = (messageId, text, setInputValue) => {
  return async (dispatch, store) => {
    const { messages } = store().chat;
    try {
      fetch(`http://localhost:3050/api/messages/${messageId}`, {
        method: 'PUT',
        body: JSON.stringify({ text }),
        headers: { 'Content-type': 'application/json; charset=UTF-8' },
      }).then((response) => response.json());
      if (messageId) {
        const currentMessage = messages.filter(
          (message) => message.id === messageId
        );
        setInputValue(currentMessage.text);
        dispatch(editMessage(messageId, text));
      }
      dispatch(dropCurrentMessageId());
      dispatch(toggleEditModal());
      setInputValue('');
    } catch (err) {
      console.log(err);
    }
  };
};

export const openModal = (e) => {
  return (dispatch, store) => {
    const code = e.keyCode;
    const { messages, editModal } = store().chat;
    const ownMessages = messages.filter((message) => message.own);
    const lastOwnMessage = ownMessages[ownMessages.length - 1];

    if (editModal && code === 27) {
      e.preventDefault();

      dispatch(dropCurrentMessageId());
      dispatch(toggleEditModal());
    }

    if (code === 38 && lastOwnMessage) {
      e.preventDefault();

      dispatch(setCurrentMessageId(lastOwnMessage.id));
      dispatch(toggleEditModal());
    }
  };
};
