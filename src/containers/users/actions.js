import { ADD_USER, UPDATE_USER, DELETE_USER, LOAD_USERS } from './actionTypes';
import { v4 as uuidv4 } from 'uuid';
import { togglePreloader } from '../Chat/actions';

export const loadUsers = (data) => ({
  type: LOAD_USERS,
  payload: {
    data,
  },
});

export const addUser = (data, id) => ({
  type: ADD_USER,
  payload: {
    id: id,
    data,
  },
});

export const updateUser = (id, data) => ({
  type: UPDATE_USER,
  payload: {
    id,
    data,
  },
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: {
    id,
  },
});

export const loadUsersMiddleware = () => {
  return async (dispatch, store) => {
    try {
      const { preloader } = store().chat;
      if (!preloader) {
        dispatch(togglePreloader());
      }
      const response = await fetch('http://localhost:3050/api/users');
      const data = await response.json();

      if (data.length === 0) {
        return;
      }

      dispatch(loadUsers(data));
      dispatch(togglePreloader());
    } catch (err) {
      console.log(err);
    }
  };
};

export const addUserMiddleware = (data) => {
  return async (dispatch) => {
    const userId = uuidv4();
    const message = {
      id: userId,
      login: data.login,
      password: data.password,
    };
    try {
      if (data.login.trim()) {
        fetch('http://localhost:3050/api/users', {
          method: 'POST',
          body: JSON.stringify(message),
          headers: { 'Content-type': 'application/json; charset=UTF-8' },
        }).then((response) => response.json());
        dispatch(addUser(message, userId));
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const deleteUserMiddleware = (userId) => {
  return async (dispatch, store) => {
    try {
      dispatch(togglePreloader());

      fetch(`http://localhost:3050/api/users/${userId}`, {
        method: 'DELETE',
      }).then((response) => response.json());

      dispatch(deleteUser(userId));
      dispatch(togglePreloader());
    } catch (err) {
      console.log(err);
    }
  };
};

export const editUserMiddleware = (userId, data) => {
  return async (dispatch, store) => {
    try {
      fetch(`http://localhost:3050/api/users/${userId}`, {
        method: 'PUT',
        body: JSON.stringify({ ...data }),
        headers: { 'Content-type': 'application/json; charset=UTF-8' },
      }).then((response) => response.json());
    } catch (err) {
      console.log(err);
    }
  };
};
