import React from 'react';

const UserItem = ({ id, login, onEdit, onDelete }) => {
  return (
    <div className="container list-group-item">
      <div className="row">
        <div className="col-8">
          <span
            className="badge badge-secondary float-left"
            style={{ fontSize: '2em', margin: '2px', color: '#333' }}
          >
            {login}
          </span>
        </div>
        <div className="col-4 btn-group">
          <button
            className="btn btn-outline-primary"
            onClick={(e) => onEdit(id)}
          >
            {' '}
            Edit{' '}
          </button>
          <button
            className="btn btn-outline-dark"
            onClick={(e) => onDelete(id)}
          >
            {' '}
            Delete{' '}
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserItem;
