import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserItem from './UserItem';
import * as actions from './actions';
import { setCurrentUserId, showPage } from '../userPage/actions';
import { Redirect } from 'react-router-dom';
import Preloader from '../../shared/components/Preloader/Preloader';

const UserList = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users);
  const preloader = useSelector((state) => state.chat.preloader);
  const isShown = useSelector((state) => state.userPage.isShown);

  useEffect(() => {
    dispatch(actions.loadUsersMiddleware());
  }, [dispatch]);

  const onEdit = (id) => {
    dispatch(setCurrentUserId(id));
    dispatch(showPage());
  };

  const onDelete = (id) => {
    dispatch(actions.deleteUserMiddleware(id));
  };

  const onAdd = () => {
    dispatch(showPage());
  };

  return (
    <div className="row g-0 d-flex justify-content-between">
      {isShown ? <Redirect to="/user-edit" /> : null}

      {!preloader ? (
        <div className="list-group col-10">
          {users.map((user) => {
            return (
              <UserItem
                key={user.id}
                id={user.id}
                login={user.login}
                onEdit={onEdit}
                onDelete={onDelete}
              />
            );
          })}
        </div>
      ) : (
        <Preloader />
      )}

      <div className="col-auto">
        <button
          className="btn btn-success"
          onClick={onAdd}
          style={{ margin: '5px' }}
        >
          Add user
        </button>
      </div>
    </div>
  );
};

export default UserList;
